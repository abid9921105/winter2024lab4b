import java.util.Scanner;

public class VirtualPetApp{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		Raccoon[] smackOfRaccoons = new Raccoon[1];
		
		for(int i = 0; i < smackOfRaccoons.length; i++){
			//smackOfRaccoons[i] = new Raccoon();
			
			System.out.println("What shall raccoon number:" + i + " be named?");
			String name = reader.nextLine();
			
			System.out.print("What colour shall raccoon number: " + i + " bear\r\n");
			String colour = reader.nextLine();
			
			System.out.print("How many stripes will raccoon number: " + i + " have on his tail\r\n");
			int numberOfStripesOnTail = Integer.parseInt(reader.nextLine());
			
			smackOfRaccoons[i] = new Raccoon(name, colour, numberOfStripesOnTail);
		}
		
		
		System.out.println(smackOfRaccoons[smackOfRaccoons.length-1].getName());
		System.out.println(smackOfRaccoons[smackOfRaccoons.length-1].getColour());
		System.out.println(smackOfRaccoons[smackOfRaccoons.length-1].getNumberOfStripesOnTail());
		
		smackOfRaccoons[smackOfRaccoons.length-1].setNumberOfStripesOnTail(Integer.parseInt(reader.nextLine()));
		
	    System.out.println(smackOfRaccoons[smackOfRaccoons.length-1].getName());
		System.out.println(smackOfRaccoons[smackOfRaccoons.length-1].getColour());
		System.out.println(smackOfRaccoons[smackOfRaccoons.length-1].getNumberOfStripesOnTail());
		
	    smackOfRaccoons[0].doHypnosis();
		smackOfRaccoons[0].doRobbery();
	}
}