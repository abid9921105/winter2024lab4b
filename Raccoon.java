public class Raccoon{
	private String name;
	private String colour;
	private int numberOfStripesOnTail;
	
	public Raccoon(String name, String colour, int numberOfStripesOnTail){
		this.name = name;
		this.colour = colour;
		this.numberOfStripesOnTail = numberOfStripesOnTail;
	}
	
	public void setName(String name){
		this.name = name;
	}
	public void setColour(String colour){
		this.colour = colour;
	}
	public void setNumberOfStripesOnTail(int numberOfStripesOnTail){
		this.numberOfStripesOnTail = numberOfStripesOnTail;
	}
	public String getName(){
		return this.name;
	}
	public String getColour(){
		return this.colour;
	}
	public int getNumberOfStripesOnTail(){
		return this.numberOfStripesOnTail;
	}
	
	
	
	public void doHypnosis(){
		System.out.println("THE RACCOON HYPNOTIZES YOU WITH HIS " + this.numberOfStripesOnTail + " STRIPES ON HIS TAIL! YOU ARE LEFT CONFUSED AND HYPNOTIZED." + "you can only remember one word from the incident: " + this.name + " ...");
	}
	public void doRobbery(){
		System.out.println("THE " + this.colour + " RACCOON CAMOUFLAGES HIMSELF WITH THE " + this.colour + " WALLS AND SURPRISES YOU WITH HIS CLAWS, HE TAKES ALL OF YOUR MARSHMELLOWS! It says: YOU WERE ROBBED BY THE GREAT " + this.name + ". You are left dazed and confused.");
	}
}